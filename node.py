# usr/bin/python
# -*-coding:utf-8-*-
__author__ = 'yoshidakouji'

import  warnings
import random
import numpy as np
import decimal

"""ノードを構成するクラス"""
class Node(object):

    """ノードのコンストラクタ"""
    def __init__(self,min,max):
        self.__value = random.uniform(min,max)

    """valueを応答する"""
    def get_value(self):
        return self.__value

    """valueを束縛する"""
    def value(self,value):
        self.__value = value

    """値xをシグモイド計算したものを返す"""
    def sigmoid(self,x):
        #print "*** sigmoid ***"
        x = float(x)
        minus_x = decimal.Decimal(-x)
        one = decimal.Decimal(1.0)
        result = decimal.Decimal(one/(one+minus_x.exp()))
        #print float(result)
        return float(result)
# usr/bin/python
# -*-coding:utf-8-*-
__author__ = 'yoshidakouji'

import node

"""入力層のノードを構成するクラス"""
class Input_node(node.Node):

    """Input_nodeのコンストラクタ"""
    def __init__(self,min,max):
        """:rtype : object"""
        #print "input_node"
        super(Input_node,self).__init__(min,max)
        self.__value = super(Input_node,self).get_value()
        self.__output_value = self.__value
        self.__to_lines = []

    """to_linesにlineを追加する"""
    def add_to_lines(self,to_line):
        self.__to_lines.append(to_line)

    """出力値を応答する"""
    def get_output_value(self):
        return self.__output_value

    """次に接続されているlineに値を伝達する"""
    def transmit(self,input):
        self.__output_value = input
        #print "input - transmit"
        for a_line in self.__to_lines:
            a_line.transmit()

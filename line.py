# usr/bin/python
# -*-coding:utf-8-*-
__author__ = 'yoshidakouji'

import random

"""ノード同士を接続するLineを構成するクラス"""
class Line(object):

    """Lineのコンストラクタ"""
    def __init__(self,min,max,from_node,to_node):
        self.__strong = random.uniform(min,max)
        self.__from_node = from_node
        self.__to_node = to_node

    """from_nodeにnodeを束縛する"""
    def set_from_node(self,from_node):
        self.__from_node = from_node

    """to_nodeにnodeを束縛する"""
    def set_to_node(self,to_node):
        self.to_node = to_node

    """from_nodeを応答する"""
    def from_node(self):
        return self.__from_node

    """to_nodeを応答する"""
    def to_node(self):
        return self.__to_node

    """次のノードに結合強度をかけた値を伝達する"""
    def transmit(self):
        #print "line - transmit"
        self.__to_node.input_value(self.__from_node.get_output_value() * self.__strong)
        self.__to_node.transmit()
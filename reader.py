# usr/bin/python
# -*-coding:utf-8-*-
__author__ = 'yoshidakouji'

import io

"""csv読み込み用クラス"""
class Reader(io.IO):

    """クラスReaderのコンストラクタ"""
    def __init__(self,csv_filename):
        self.__csv_filename = csv_filename

    """"csvを読み込む"""
    def read_csv(self):
        return super(Reader,self).read_csv(self.__csv_filename)
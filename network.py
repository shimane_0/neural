# usr/bin/python
# -*-coding:utf-8-*-
__author__ = 'yoshidakouji'

import operator

import input_node
import middle_node
import output_node
import line

import input_translator

"""擬似3層ニューラルネットワークを構成するクラス"""
class Network(object):

    """Networkのコンストラクタ"""
    def __init__(self,input_layer,middle_layer,output_layer,min,max):
        """:rtype : object"""
        #print "*** network ***"
        self.__input_layer = input_layer
        self.__middle_layer = middle_layer
        self.__output_layer = output_layer
        self.__min = min
        self.__max = max
        self.__input_nodes = []
        self.__middle_nodes = []
        self.__output_nodes = []
        self.create()

    """node群を作成する"""
    def __create_nodes(self):
        print "*** create ***"
        for index in range(0,self.__input_layer):
            self.__input_nodes.append(input_node.Input_node(self.__min,self.__max))
        #print self.__input_nodes
        for index in range(0,self.__middle_layer):
            self.__middle_nodes.append(middle_node.Middle_node(self.__min,self.__max))
        #print self.__middle_nodes
        for index in range(0,self.__output_layer):
            self.__output_nodes.append(output_node.Output_node(self.__min,self.__max))
        #print self.__output_nodes

    """lineを作成し、nodeとnodeを接続する"""
    def __attach_lines(self):
        #print "*** attach ***"
        for from_node in self.__input_nodes:
            for to_node in self.__middle_nodes:
                a_line = line.Line(self.__min,self.__max,from_node,to_node)
                from_node.add_to_lines(a_line)
                to_node.add_from_lines(a_line)
        for from_node in self.__middle_nodes:
            for to_node in self.__output_nodes:
                a_line = line.Line(self.__min,self.__max,from_node,to_node)
                from_node.add_to_lines(a_line)
                to_node.add_from_lines(a_line)

    """Networkを作成する"""
    def create(self):
        self.__create_nodes()
        self.__attach_lines()

    """リストinput_nodesを応答する"""
    def input_nodes(self):
        return self.__input_nodes

    """リストmiddle_nodesを応答する"""
    def middle_nodes(self):
        return self.__middle_nodes

    """リストoutput_nodesを応答する"""
    def output_nodes(self):
        return self.__output_nodes

    """ニューラルネットに入力を入れ、出力層まで伝達させる"""
    def transmit(self,input):
        #inputはListの為、バラして渡さなければならない
        count = 0
        try:
            for a_node in self.__input_nodes:
                a_node.transmit(input[count])
                #print "input [count]"
                #print input[count]
                count = count + 1
        except IndexError:
            print "IndexError! 入力層数が"+str(count)+"以上ありませんか？"
        return self.__output_nodes

    """テストし、最も値の大きい出力層のindexを応答する"""
    def test(self,input):
        an_output_nodes = self.transmit(input)
        #print an_output_nodes
        index = self.index_of_output(an_output_nodes)
        #print index
        return index

    """一番大きい出力層のindexを参照し、それを答えとして応答する"""
    def index_of_output(self,an_output_nodes):
        count = 0
        first = True
        for a_node in an_output_nodes:
            if first==True:
                max = a_node.output_value()
                index = count
                first = False
            elif max < a_node.output_value():
                max = a_node.output_value()
                index = count
            count = count+1
        return index
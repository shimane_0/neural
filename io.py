# usr/bin/python
# -*-coding:utf-8-*-
__author__ = 'yoshidakouji'

import csv

"""入出力のスーパークラス"""
class IO(object):

    """csvファイルを読み込むメソッド"""
    def read_csv(self,filename):
        print "*** read_csv ***"
        return csv.reader(open(filename, 'rU'), delimiter=',', quotechar='"')

    """csvファイルに書き込むメソッド"""
    def write_csv(self,filename):
        print "*** write_csv ***"
        csv.writer(open(filename, 'wb'), delimiter=',', quotechar='|', quoting = csv.QUOTE_MINIMAL)
        return
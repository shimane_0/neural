# usr/bin/python
# -*-coding:utf-8-*-
__author__ = 'yoshidakouji'

import node

"""中間層のノードを構成するクラス"""
class Middle_node(node.Node):

    """Middle_nodeのコンストラクタ"""
    def __init__(self,min,max):
        """:rtype : object"""
        #print "middle_node"
        super(Middle_node,self).__init__(min,max)
        self.__value = super(Middle_node,self).get_value()
        self.__input_value = 0.0
        self.__output_value = 0.0
        self.__from_lines = []
        self.__to_lines = []

    """from_linesにlineを追加する"""
    def add_from_lines(self,from_line):
        self.__from_lines.append(from_line)

    """to_linesにlineを追加する"""
    def add_to_lines(self,to_line):
        self.__to_lines.append(to_line)

    """入力値を足しあわせる"""
    def input_value(self,value):
        self.__input_value += value

    """出力値を応答する"""
    def get_output_value(self):
        return self.__output_value

    """次に接続されているlineに値を伝達する"""
    def transmit(self):
        #print "middle - transmit"
        self.__output_value = super(Middle_node,self).sigmoid(self.__input_value)+self.__value
        for a_line in self.__to_lines:
            a_line.transmit()
# usr/bin/python
# -*-coding:utf-8-*-
__author__ = 'yoshidakouji'

import re

"""入力データ解釈用のクラス"""
class Input_translator(object):

    """入力データを解釈して返却する。int or Stringが結果でfloatを入力と取るものを受理 引数1以上、結果1に対応"""
    def translate(self,input):
        translated = []
        #print "*** translate ***"
        for data in input:
            searched = re.findall("[0-9]+[.][0-9]+",data)
            #print "searched"
            #print searched
            #print len(searched)
            if len(searched)==1:
                float_input = float(searched[0])
                translated.append(float_input)
            else :
                #print len(searched)
                #print len(data)
                searched = re.findall("[0-9]+",data)
                if len(searched)==1:
                    self.__result = int(searched[0])
                else:
                    self.__result = str(searched[0])
        #print "translated"
        return translated

    """結果の値を応答する"""
    def result(self):
        return self.__result
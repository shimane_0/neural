# usr/bin/python
# -*-coding:utf-8-*-
__author__ = 'yoshidakouji'

import network
import reader
import input_translator

"""ニューラルネットワークのメインクラス"""
class Neural(object):

    def __init__(self):
        print "*** Neural ***"
        self.__an_input_translator = input_translator.Input_translator()
        self.__read = self.__read_data("data/data01.csv")

    """メインメソッド"""
    def main(self):
        """:rtype : object"""
        print "*** neural ***"
        a_network = network.Network(2,5,2,-10,10)
        accuracy_rate = self.in_data(a_network,self.__read)
        print "正答率"+str(accuracy_rate)+"%"

    """入力データを解釈させて、返す"""
    def translate(self,input):
        translated_input = self.__an_input_translator.translate(input)
        result = self.__an_input_translator.result()
        return translated_input

    """データをネットワークに入れ、正答率を返させる"""
    def in_data(self,a_network,read):
        data_count = 0.0
        accuracy = 0.0
        for input in read:
            index = a_network.test(self.translate(input))
            print "正解 : "+str(self.__an_input_translator.result())
            print "結果 : "+str(index)
            print "- - - - - "
            self.__result = self.__judgment(self.__an_input_translator.result(),index)
            if self.__result == True:
                accuracy = accuracy + 1
                #print accuracy
            data_count = data_count + 1
            #print data_count
        accuracy_rate = self.__compute_accuracy_rate(accuracy,data_count)
        return accuracy_rate

    """正答率を計算する"""
    def __compute_accuracy_rate(self,accuracy,data_count):
        accuracy = accuracy * 1.0
        data_count = data_count * 1.0
        accuracy_rate = round(accuracy/data_count*100,1)
        return accuracy_rate

    """データと出力結果の合否を判定し、合/True 否/Falseを返す"""
    def __judgment(self,correct_answer,answer):
        if str(correct_answer) == str(answer):
            result = True
        else:
            result = False
        return result

    """文字列で指定されたcsvファイルを読み込み、オブジェクトを応答する"""
    def __read_data(self,csv_string):
        a_reader = reader.Reader(csv_string)
        read = a_reader.read_csv()
        self.data_count = read.line_num
        return read
# usr/bin/python
# -*-coding:utf-8-*-
__author__ = 'yoshidakouji'

import random

import node

"""出力層のノードを構成するクラス"""
class Output_node(node.Node):

    """Output_nodeのコンストラクタ"""
    def __init__(self,min,max):
        """:rtype : object"""
        #print "output_node"
        super(Output_node,self).__init__(min,max)
        self.__value = super(Output_node,self).get_value()
        self.__input_value = 0.0
        self.__output_value = 0.0
        self.__from_lines = []

    """from_linesにlineを追加する"""
    def add_from_lines(self,from_line):
        self.__from_lines.append(from_line)

    """入力値を足し合わせる"""
    def input_value(self,value):
        self.__input_value += value

    """出力値を応答する"""
    def output_value(self):
        return self.__output_value

    """中間層からの出力値を入力として受け取り、計算し、このノードの出力値として束縛する"""
    def transmit(self):
        #print "output - transmit"
        self.__output_value = super(Output_node,self).sigmoid(self.__input_value)+self.__value
        #print self.__output_value